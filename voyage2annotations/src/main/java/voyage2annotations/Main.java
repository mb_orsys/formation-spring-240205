package voyage2annotations;

import java.time.LocalDate;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

public class Main {

	public static void main(String[] args) {
		AbstractApplicationContext contexte =
				new AnnotationConfigApplicationContext(
						"voyage2annotations");
		contexte.registerShutdownHook();
		System.out.println("* Mexique : ");
		
		MexiqueService mService = contexte.getBean("mexiqueService", MexiqueService.class);
		System.out.println(mService.getMessage());
		
		MexiqueService mService2 = contexte.getBean("mexiqueService", MexiqueService.class);
		System.out.println(mService2.getMessage());

		System.out.println("Aujourd'hui : "+contexte.getBean("maintenant", LocalDate.class));
	}

}
