package voyage2annotations;

import java.time.LocalDate;

import javax.annotation.PostConstruct;
// import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.beans.factory.annotation.Qualifier;

public class MexiqueService {

	private LocalDate maintenant;
	
	@Autowired
	private ConseilService conseilService;
	
//	@Resource
	@Autowired
	@Qualifier("maintenant")
	public void setMaintenant(LocalDate maint) {
		this.maintenant = maint;
	}

	public String getMessage() {
		if(maintenant.isBefore(LocalDate.of(2028,1,1)))
			return conseilService.getJoursConseilles()+" jours conseillés. "+
					"Pas encore de voyage pour le mexique";
		else 
			return "Plus de voyage pour le mexique";
	}
	
	@PostConstruct public void init() {
		System.out.println("*** init Mexique ***");
	}

}
