package voyage2annotations;

import java.time.LocalDate;
import java.util.Random;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;

@Configuration
@EnableAspectJAutoProxy
public class Config {
	@Bean
	@Lazy
	// @Scope("singleton")
	@Scope("prototype")
	public MexiqueService mexiqueService() {
		return new MexiqueService();
	}
	
	@Bean
	public LocalDate maintenant() {
		return LocalDate.now();
	}
	
	@Bean
	public Double prix() { return 0.0; }
	
	@Bean
	@Scope("prototype")
	public Integer joursConseilles() { 
		return 5 + new Random().nextInt(20);
	}
}
