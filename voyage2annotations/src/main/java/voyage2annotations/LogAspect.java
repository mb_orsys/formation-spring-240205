package voyage2annotations;

import java.time.LocalDateTime;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LogAspect {
	// @Before("execution(java.lang.String voyage2annotations.ConseilService.getMessage())")
	// @Before("execution(* getMessage())")
	// @Before("execution(* get*())")
	// @Before("execution(* voyage2annotations..*.*(..))")
	public void afficheDebut(JoinPoint joinPoint) {
		System.out.println(" # "+LocalDateTime.now()+" Début de "+joinPoint.getSignature().getName());
	}
}
