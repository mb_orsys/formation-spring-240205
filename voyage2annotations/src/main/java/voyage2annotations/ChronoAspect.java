package voyage2annotations;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class ChronoAspect {

	@Around("execution(* getMessage())")
	public Object afficherDuree(ProceedingJoinPoint pjp) throws Throwable {
		long debut = System.nanoTime();
		Object resultat = pjp.proceed();
		long fin = System.nanoTime();
		long totalUs = (fin-debut)/1000;
		System.out.println("# Execution de "+pjp.getSignature().getName()+" : "+totalUs+"µs");
		return resultat;
	}
	
}
