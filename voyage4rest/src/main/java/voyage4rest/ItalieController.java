package voyage4rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Positive;

@RestController
@RequestMapping("/italie")
@Validated
public class ItalieController {
	
	Logger log = LoggerFactory.getLogger(ItalieController.class);
	
	private int avion;

	public ItalieController() {
		avion = 890;
	}
	
	// @RequestMapping(method = RequestMethod.GET)
	@GetMapping(path="/avion", produces = MediaType.APPLICATION_JSON_VALUE)
	public int getAvion() {
		return avion;
	}

	@PutMapping(path="/avion", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void setAvion(@RequestBody @Positive @Max(value = 5000, 
			message = "Pas d'avion a plus de 5000€") int avion) {
		this.avion = avion;
		log.info("Nouveau prix d'avion Italie : "+avion);
	}
	
	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<String> gereValidation(ConstraintViolationException ex) {
		return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(ex.getMessage());
	}
}
