package voyage4rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.ConstraintViolationException;

@RestController
@RequestMapping("/espagne")
public class EspagneController {
	
	@RequestMapping("")
	public String afficherEspagne() {
		return "Espagne ok";
	}
	
	@RequestMapping(path = "/conditions", produces = "application/json; charset=utf-8")
	public String[] afficherConditions() {
		return new String[]
				{ "Retour non assuré", "Logement en camping", "Déplacements non garantis" };
	}
	
	@RequestMapping(path = "/conditions.xml", produces = "application/xml")
	public String[] afficherConditionsXml() {
		return afficherConditions();
	}
	
	private String[] villes = {"Madrid", "Cordoue", "Barcelone", "Valence"};
	
	@RequestMapping(path="/villes", produces = "application/json; charset=utf-8")
	public String[] afficherVilles() {
		return villes;
	}
	
	@RequestMapping(path="/villes/{no}", produces = "text/plain; charset=utf-8")
	public String afficherVille(@PathVariable("no") int n) {
		return villes[n-1]; // 1ère ville : n°1
	}
	
	@ExceptionHandler(ArrayIndexOutOfBoundsException.class)
	public ResponseEntity<String> gereErreurTableau(ArrayIndexOutOfBoundsException ex) {
		return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(ex.getMessage());
	}
}
