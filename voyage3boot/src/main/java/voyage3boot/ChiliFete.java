package voyage3boot;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

@RedisHash("cf")
public class ChiliFete {

	private String id;
	@Indexed
	private int mois;
	private int jour;
	private String nom;
	
	public ChiliFete() {
		this(null, 1, 1, null);
	}	
	
	public ChiliFete(String id, int mois, int jour, String nom) {
		this.id = id;
		this.mois = mois;
		this.jour = jour;
		this.nom = nom;
	}

	@Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getMois() {
		return mois;
	}
	public void setMois(int mois) {
		this.mois = mois;
	}
	public int getJour() {
		return jour;
	}
	public void setJour(int jour) {
		this.jour = jour;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	
}
