package voyage3boot;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class ArgentineConfig {
	@Autowired private DataSource dataSource;
	
	@Bean
	public LocalSessionFactoryBean sessionFactoryBean() {
		LocalSessionFactoryBean lsfb = new LocalSessionFactoryBean();
		lsfb.setDataSource(dataSource);
		Properties options = new Properties();
		options.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		options.setProperty("hibernate.hbm2ddl.auto", "create");
		lsfb.setHibernateProperties(options);
		lsfb.setPackagesToScan("voyage3boot");
		return lsfb;
	}

	@Bean 
	public SessionFactory sessionFactory(LocalSessionFactoryBean lsfb) {
		return lsfb.getObject();
	}
	
	@Bean
	public PlatformTransactionManager hibernateTransactionManager(SessionFactory sessionFactory) {
		return new HibernateTransactionManager(sessionFactory);
	}
}
