package voyage3boot;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class ArgentineService {
	
	private static Logger log = LoggerFactory.getLogger(ArgentineService.class);
	
	@Autowired private SessionFactory sessionFactory;

	@Transactional
	public void insererMonuments() {
		Session session = sessionFactory.getCurrentSession();
		session.persist(new ArgentineMonument("Cafe Tortoni", "Buenos Aires"));
		ArgentineMonument m2 = new ArgentineMonument("Teatro Colon", "Buenos Aires");
		session.persist(m2);
		log.info("Last added : "+m2.getId());		
	}
}
