package voyage3boot;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="monuments")
public class ArgentineMonument implements Serializable {
	private int id;
	private String nom;
	private String ville;
	
	public ArgentineMonument() {
		this(null, null);
	}	
	public ArgentineMonument(String nom, String ville) {
		this.id = 0;
		this.nom = nom;
		this.ville = ville;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	
	
	
}
