package voyage3boot;

import java.util.Random;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@Component
public class ConseilsService {
	
	private String lireConseil() {
		String[] pays = {
				"Honduras", "Costa Rica", "Panama"
		};
		return "Nous vous conseillons chaudement le "+
			pays[new Random().nextInt(3)];
	}
	
	@Cacheable(value="conseils",
			key="{#a0}")
	public String getConseil(int jours) {
		return lireConseil();
	}
	
	// @CacheEvict(value="conseils")
	@CachePut(value="conseils",
			key="{#a0}")
	public String getNouveauConseil(int jours) {
		return lireConseil();
	}
}
