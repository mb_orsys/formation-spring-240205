package voyage3boot;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Configuration
public class PerouService {

	@Value("${voyage3boot.accueil-perou}")
	private String accueil;
	
	@Transactional(/*readOnly = true*/)
	public void afficherLieux() {
		System.out.println(accueil);
		NamedParameterJdbcTemplate template = perouJdbcTemplate();
		String insertSql = "INSERT INTO lieux(nom, region) VALUES (:n, :r)";
		Map<String, String> valeurs1 = Map.of("n", "Arica", "r", "Tacna");
		template.update(insertSql, valeurs1);
		Map<String, String> valeurs2 = Map.of("n", "Iquitos", "r", "Loreto");
		template.update(insertSql, valeurs2);
		
		List<Map<String, Object>> lieux = template.getJdbcOperations().
				queryForList("SELECT DISTINCT nom, region FROM lieux ORDER BY nom");
		for(Map<String, Object> lieu : lieux)
			System.out.println(" - "+lieu.get("nom")+" - "+lieu.get("region"));
	}
	
	@Autowired DataSource dataSource;
	
	@Bean
	public NamedParameterJdbcTemplate perouJdbcTemplate() {
		return new NamedParameterJdbcTemplate(dataSource);		
	}
	
	/*
	// Configuré dans application.yaml
	@Bean
	public DataSource getDataSource() {
		DriverManagerDataSource dmds = new DriverManagerDataSource();
		dmds.setDriverClassName("com.mysql.cj.jdbc.Driver");
		dmds.setUrl("jdbc:mysql://localhost:3306/voyages");
		dmds.setUsername("bob");
		dmds.setPassword("1234");
		return dmds;
	} */
}
