package voyage3boot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

@Component
@Configuration
@Profile({"avec_bresil"})
public class BresilService {

	private static Logger logger = LoggerFactory.getLogger(BresilService.class);
	
	public void debutRecherche() {
		System.out.println("Debut de la recherche");
	}

	@Scheduled(fixedRate = 100, 
			scheduler = "ameriqueSudScheduler")
	public void continueRecherche() {
		System.out.print(".");
	}	
	
	final private int delai = 1000;
	
	@Scheduled(initialDelay = delai, 
			scheduler = "ameriqueSudScheduler")
	public void finRecherche() {
		System.out.println("Fin de la recherche : pas de voyage");
	}
	
	@Bean
	public TaskScheduler ameriqueSudScheduler() {
		ThreadPoolTaskScheduler sc = new ThreadPoolTaskScheduler();
		return sc;
	}
}
