package voyage3boot;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChiliRepository 
		extends CrudRepository<ChiliFete, String>{

	List<ChiliFete> findAllByOrderByMois();
	List<ChiliFete> findByMois(int mois);	
}
