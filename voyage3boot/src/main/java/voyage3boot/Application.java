package voyage3boot;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.context.annotation.Profile;
import org.springframework.jmx.export.MBeanExporter;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@SpringBootApplication
@EnableCaching
@EnableMBeanExport
@EnableScheduling
public class Application implements CommandLineRunner {

	private static Logger logger = LoggerFactory.getLogger(Application.class);
	
	@Autowired ConseilsService conseilsService;
	@Autowired(required = false) BresilService bresilService;
	@Autowired(required = false) TaskScheduler ameriqueSudScheduler;
	@Autowired PerouService perouService;
	@Autowired ArgentineService argentineService;
	@Autowired ChiliService chiliService;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	@Bean
	public CacheManager cacheManager() {
		return new ConcurrentMapCacheManager("conseils");
	}
	
	@Override
	public void run(String... args) throws Exception {
		logger.info("Voyages en Amérique latine");
		logger.info(conseilsService.getConseil(8)); // Honduras
		logger.info(conseilsService.getConseil(8)); // Honduras
		logger.info(conseilsService.getNouveauConseil(8)); // Panama
		logger.info(conseilsService.getConseil(8)); // Panama
		logger.info(conseilsService.getConseil(8)); // Panama
		logger.info(conseilsService.getConseil(23)); // Costa Rica
		logger.info(conseilsService.getConseil(8)); // Panama
		// test mbeans Thread.sleep(20000);
		
		if(bresilService!=null) {
			bresilService.debutRecherche();
			// scheduled bresilService.finRecherche();
		}
		
		// Arrêt après 1 seconde :		
		if(ameriqueSudScheduler!=null) {
			ameriqueSudScheduler.schedule( ()->{ 
				((ThreadPoolTaskScheduler) ameriqueSudScheduler).shutdown(); 
			}, 
			LocalDateTime.now().plusSeconds(1).toInstant(
				ZoneId.systemDefault().getRules().getOffset(LocalDateTime.now())));
		}
		
		// JDBC 
		// perouService.afficherLieux();
		// Hibernate
		argentineService.insererMonuments();
		// REDIS 
		// chiliService.afficherFetes();
	}

	@Bean
	@Profile("avec_jmx")
	public MBeanExporter getExporter() {
		Map<String, Object> beans = new HashMap<>();
		beans.put("voyage:name=conseils", "conseilsService");
		MBeanExporter exporter = new MBeanExporter();
		exporter.setBeans(beans);
		return exporter;
	}
	

}
