package voyage3boot;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

@Configuration
@EnableRedisRepositories("voyage3boot")
public class ChiliConfig {

	private JedisConnectionFactory factory;
	
	@Bean
	public RedisConnectionFactory redisConnectionFactory() {
		if(factory==null) {
			RedisStandaloneConfiguration conf = 
				new RedisStandaloneConfiguration("localhost", 6379);
			factory =new JedisConnectionFactory(conf);
		}
		return factory;
	}
	
	@Bean
	public RedisTemplate<String, String> redisTemplate() {
		RedisTemplate<String, String> template = new RedisTemplate<>();
		template.setConnectionFactory(redisConnectionFactory());
		return template;
	}
}
