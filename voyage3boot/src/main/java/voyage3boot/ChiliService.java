package voyage3boot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ChiliService {

	@Autowired private ChiliRepository chiliRepository;
	
	public void afficherFetes() {
		chiliRepository.save(new ChiliFete("SP", 6, 28, "Fiesta de San Pedro"));
		chiliRepository.save(new ChiliFete("C", 2, 2, "Fiesta de la Candelaria"));
		chiliRepository.save(new ChiliFete("LC", 8, 25, "Limpieza de Canales"));
		for(ChiliFete f : chiliRepository.findAllByOrderByMois())
			System.out.println(" - "+f.getNom());
		System.out.println("En juin : ");
		for(ChiliFete f : chiliRepository.findByMois(6))
			System.out.println(" - "+f.getNom());
	}
}
