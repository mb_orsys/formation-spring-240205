package voyage3boot;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ConseilsServiceTest {

	@Autowired ConseilsService conseilsService;
	
	@Test
	public void testGetConseil() {
		String conseil0 = conseilsService.getConseil(0);
		assertTrue(conseil0.length() > 15);
	}
}
