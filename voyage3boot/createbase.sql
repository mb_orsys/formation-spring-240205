create database voyages;
create user 'bob'@'localhost' identified with mysql_native_password by '1234';
create user 'bob'@'%' identified with mysql_native_password by '1234';
grant all privileges on voyages.* to 'bob'@'localhost';
grant all privileges on voyages.* to 'bob'@'%';

use voyages;
create table lieux(id INTEGER PRIMARY KEY AUTO_INCREMENT, nom VARCHAR(50), region VARCHAR(50));
desc lieux;