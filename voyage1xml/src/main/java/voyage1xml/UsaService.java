package voyage1xml;

import java.time.Duration;
import java.time.LocalDate;

public class UsaService {	
	
	private double avion, hotel, taxes;
	private LocalDate maintenant;
	
	public UsaService(double avion, double hotel) {
		this.avion = avion;
		this.hotel = hotel;
	}
	
	public void setTaxes(double taxes) {
		this.taxes = taxes;
	}

	public void setMaintenant(LocalDate maintenant) {
		this.maintenant = maintenant;
	}

	public double getPrix(int jours) {
		return avion + jours * hotel + taxes;
	}
	
	public String getConfort() {
		if(Math.abs(maintenant.getMonthValue()-8)<2)
			return "Voyages en bus climatisé à l'avant";
		else
			return "Voyages en bus standard";
	}
	
	public void init() { 
		System.out.println("*** init Usa ***");
	}
}
