package voyage1xml;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.Resource;

public class Main {

	public static void main(String[] args) {
		System.out.println("Bienvenue - voyages en Alaska");
		// Où ? : classpath: , file://c:\\..., http://...
		AbstractApplicationContext contexte = 
				new FileSystemXmlApplicationContext("classpath:spring.xml");
		contexte.registerShutdownHook();
		
		Resource texteAlaska = contexte.getResource("classpath:alaska.txt");	
		try {
			InputStream isAlaska = texteAlaska.getInputStream();
			BufferedReader brAlaska = new BufferedReader(new 
					InputStreamReader(isAlaska, StandardCharsets.UTF_8));
			String ligne= "";
			while( (ligne = brAlaska.readLine()) != null)
				System.out.println(ligne);
			brAlaska.close();
			isAlaska.close();
		} catch(IOException ex) {
			System.err.println("Erreur lecture resource : "+ex);
		}
		
		UsaService usaService1 = new UsaService(789.90, 127.0);
		System.out.println("Prix des USA pour 10 jours : "+usaService1.getPrix(10));
		
		UsaService usaService2 = contexte.getBean("usaService2", UsaService.class);
		System.out.println("Prix des USA pour 12 jours : "+usaService2.getPrix(12));
		
		String confortUsa = contexte.getBean("confortUsa", String.class);
		System.out.println("Confort américain : "+confortUsa);
		
		System.out.println("Aujourd'hui : "+
				contexte.getBean("maintenant", LocalDate.class));
	}

}
