package voyage5webflux;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/namibie")
public class NamibieService {

	@GetMapping("/arrivee")
	public Ville getArrivee() {
		return new Ville("Windoek", 5);
	}
	
	private Flux<Ville> getVilles() {
		return Flux.just(getArrivee(), 
				new Ville("Keetmanshoop", 3),
				new Ville("Walvis Bay", 4));
	}
	
	@GetMapping("/villes")
	public Flux<Ville> lireVilles() {
		return getVilles();
	}
	
	@GetMapping("/villes/{no}")
	public Mono<Ville> lireVille(@PathVariable("no") int n) {
		return getVilles().elementAt(n);
	}
}
