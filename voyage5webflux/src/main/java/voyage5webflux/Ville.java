package voyage5webflux;

public class Ville {
	public String nom;
	public int jours;
	
	public Ville() {
		this(null, 0);
	}
	
	public Ville(String nom, int jours) {
		super();
		this.nom = nom;
		this.jours = jours;
	}	
}
