package voyage6mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/japon")
public class JaponController {
	@GetMapping
	public String getPage() {
		return "japon";
	}
}
