package voyage6mvc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class ChatStompController {
    Logger logger = LoggerFactory.getLogger(ChatStompController.class);
    
	@GetMapping("/chat_stomp")
	public String getPage() {
		return "chat_stomp";
	}

	@MessageMapping("/echo")
	@SendTo("/topic/chat01")
	public String echo(String message) {
		logger.info("Echo : "+message );
		return "Ok : " + message;
	}
}
