package voyage6mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/inde")
public class IndeController {
	
	@GetMapping({"/", "/accueil"})
	public String afficherAccueil() {
		return "accueil";
	}
	
	private String[] destinations = {"Bangalore", "Kerala", "New Deli"};
	
	@GetMapping({"/destinations"})
	public String afficherDestinations(Model model) {
		model.addAttribute("destinations", destinations);
		return "destinations";
	}
	
	@GetMapping({"/destination"})
	public String afficherDestination(Model model, @RequestParam("n") int n) {
		model.addAttribute("destination", destinations[n]);
		return "destination";
	}
	
}
