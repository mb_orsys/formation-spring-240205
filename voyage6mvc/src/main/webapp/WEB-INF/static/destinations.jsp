<%@taglib uri="jakarta.tags.core" prefix="c" %>
<!DOCTYPE html>
<html>
<head></head>
<body>
	<h1>Voyages en Inde(s)</h1>
	<c:forEach items="${destinations}" var="d" varStatus="vs">
		<p><a href="./destination?n=${vs.index}">- ${d}</a></p>
	</c:forEach>
</body>
</html>