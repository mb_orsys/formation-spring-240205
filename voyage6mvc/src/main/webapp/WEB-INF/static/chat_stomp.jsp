<html><head>
<script type="importmap">
  {
    "imports": {
      "@stomp/stompjs": "https://ga.jspm.io/npm:@stomp/stompjs@7.0.0/esm6/index.js"
    }
  }
</script>
<script async src="https://ga.jspm.io/npm:es-module-shims@1.5.1/dist/es-module-shims.js" crossorigin="anonymous"></script>
</head>
<body>
	<h2>WebSockets + STOMP</h2>
	<p>
		Message <input id="message"><input type="button" id="envoi" value="Envoyer">
	</p>
	<p id="contenu"></p>
<script type="module">
import { Client } from '@stomp/stompjs';

const client = new Client({
  brokerURL: 'ws://localhost:8080/chat_stomp/messageHandler',
  onConnect: function () {
    document.getElementById("contenu").innerHTML += "Open"
    client.subscribe('/topic/chat01', function(message) {
      document.getElementById("contenu").innerHTML += "<br>"+message.body
    })      
  } 
});

client.onWebSocketError = function(error) {
    document.getElementById("contenu").innerHTML += "WebSocket error : "+error
}
client.onStompError  = function(frame) {
    document.getElementById("contenu").innerHTML += "Stomp error : "+frame.headers['message']
}

document.getElementById("envoi").onclick = function() {
	client.publish({ destination: '/app/echo', 
		body: document.getElementById("message").value });
	document.getElementById("message").value = ""
}

client.activate();
</script>