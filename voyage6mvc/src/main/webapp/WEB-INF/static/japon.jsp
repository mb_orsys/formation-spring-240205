<!DOCTYPE html>
<html>
<head></head>
<body>
	<h1>Japon - notre webchat</h1>
	<p>
		<input id="message">
		<input type="button" id="envoi" value="Envoyer">
	</p>
	<p id="contenu"></p>
	<script>
	var ws = new WebSocket("ws://localhost:8080/japon/messageHandler")
	var contenu = document.getElementById("contenu")
	ws.onopen = function(event) {
		contenu.innerHTML += "Bonjour !"
	}
	ws.onerror = function(event) {
		contenu.innerHTML += "Erreur !"
	}
	ws.onmessage = function(event) {
		contenu.innerHTML += "<br>"+event.data
	}
	document.getElementById("envoi").onclick = function() {
		ws.send(document.getElementById("message").value)
		document.getElementById("message").value = ""
	}	
	</script>
</body>
</html>